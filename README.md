# Logit - Log system for PHP

Logit handle your logs to files, databases and various data lakes.
Complete list of handlers is below. You can build your own special
handlers with advanced logging strategies with info at below.

This library implements
the [PSR-3](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md)
interface that you can type-hinst against in your own libraries to keep a
maximum interoperability. You can also use it in your applications to
make sure you can always use another compatible logger at a later time.

## Installation

Install the latest version with

```bash
$ composer require jantia/logit
```

## Basic Usage

```php
<?php

use Jantia\LogitLevel;
use Jantia\Logger;
use Jantia\Handler\FileHandler;

// create a log channel
$log = new Logger('name');
$log->pushHandler(new Handler('path/to/your.log', LogitLevel::Warning->value));

// add records to the log
$log->warning('My warning');
$log->error('My error');
```

## Documentation

## About

### Requirements

- Logit `^3.0` works with PHP 8.3 or above.

### Author

Jan Theon - <jan@jantheon.com>

### License

Logit is licensed under the BSD 3-Clause License - see the [LICENSE](LICENSE) file for details
