<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Logit;

//
use Psr\Log\LoggerInterface;
use Stringable;
use Tiat\Standard\Time\TimestampInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface LogitInterface extends LoggerInterface, TimestampInterface {
	
	/**
	 * Get encrypt status (will the message be encrypted before sending it outside the system)
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getEncrypt() : bool;
	
	/**
	 * Will the message be encrypted before sending it outside the system
	 *
	 * @param    bool    $status
	 *
	 * @return LogitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setEncrypt(bool $status) : LogitInterface;
	
	/**
	 * Return Logger instance name
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : string;
	
	/**
	 * Adds a log record at an arbitrary level.
	 * This method allows for compatibility with common interfaces
	 *
	 * @param    mixed                $level
	 * @param    Stringable|string    $message
	 * @param    array                $context
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function log(mixed $level, Stringable|string $message, array $context = []) : void;
	
	/**
	 * Get the channel name by Level & Name
	 *
	 * @param    LogitLevel    $level
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getChannel(LogitLevel $level) : ?string;
	
	/**
	 * Add a log record
	 *
	 * @param    LogitLevel|int    $level      The logging level
	 * @param    string            $message    The log message
	 * @param    array             $context    The log context
	 *
	 * @return bool                    Is log processed
	 * @since   3.0.0 First time introduced.
	 */
	public function addRecord(LogitLevel|int $level, string $message, array $context = []) : bool;
}
