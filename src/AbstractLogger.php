<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Logit;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractLogger extends ErrorHandler implements LogitInterface {
	
	/**
	 * Encrypt the message before sending it outside the system
	 *
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_encrypt = TRUE;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function getEncrypt() : bool {
		return $this->_encrypt;
	}
	
	/**
	 * @param    bool    $status
	 *
	 * @return LogitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setEncrypt(bool $status) : LogitInterface {
		//
		$this->_encrypt = $status;
		
		//
		return $this;
	}
	
	/**
	 * Check that the name fulfill the name regulations
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _checkName() : void;
}
