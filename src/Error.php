<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Logit;

//
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Stringable;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Error implements LoggerInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_logRecords;
	
	/**
	 * Action must be taken immediately.
	 * Example: Entire website down, database unavailable, etc. This should
	 * trigger the SMS alerts and wake you up.
	 *
	 * @param    Stringable|string    $message
	 * @param    array                $context
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function alert(Stringable|string $message, array $context = []) : void {
		$this->log(LogLevel::ALERT, (string)$message, $context);
	}
	
	/**
	 * Logs with an arbitrary level.
	 *
	 * @param    mixed                $level
	 * @param    Stringable|string    $message
	 * @param    array                $context
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function log(mixed $level, Stringable|string $message, array $context = []) : void {
		$this->_logRecords[] = ['level' => $level, 'message' => (string)$message, 'context' => $context];
	}
	
	/**
	 * Critical conditions.
	 * Example: Application component unavailable, unexpected exception.
	 *
	 * @param    Stringable|string    $message
	 * @param    array                $context
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function critical(Stringable|string $message, array $context = []) : void {
		$this->log(LogLevel::CRITICAL, (string)$message, $context);
	}
	
	/**
	 * Runtime errors that do not require immediate action but should typically
	 * be logged and monitored.
	 *
	 * @param    Stringable|string    $message
	 * @param    array                $context
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function error(Stringable|string $message, array $context = []) : void {
		$this->log(LogLevel::ERROR, (string)$message, $context);
	}
	
	/**
	 * System is unusable.
	 *
	 * @param    Stringable|string    $message
	 * @param    array                $context
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function emergency(Stringable|string $message, array $context = []) : void {
		$this->log(LogLevel::EMERGENCY, (string)$message, $context);
	}
	
	/**
	 * Exceptional occurrences that are not errors.
	 * Example: Use of deprecated APIs, poor use of an API, undesirable things
	 * that are not necessarily wrong. API error requests should be logged here.
	 *
	 * @param    Stringable|string    $message
	 * @param    array                $context
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function warning(Stringable|string $message, array $context = []) : void {
		$this->log(LogLevel::WARNING, (string)$message, $context);
	}
	
	/**
	 * Normal but significant events.
	 *
	 * @param    Stringable|string    $message
	 * @param    array                $context
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function notice(Stringable|string $message, array $context = []) : void {
		$this->log(LogLevel::NOTICE, (string)$message, $context);
	}
	
	/**
	 * Interesting events.
	 * Example: User logs in, SQL logs.
	 *
	 * @param    Stringable|string    $message
	 * @param    array                $context
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function info(Stringable|string $message, array $context = []) : void {
		$this->log(LogLevel::INFO, (string)$message, $context);
	}
	
	/**
	 * Detailed debug information.
	 *
	 * @param    Stringable|string    $message
	 * @param    array                $context
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function debug(Stringable|string $message, array $context = []) : void {
		$this->log(LogLevel::DEBUG, (string)$message, $context);
	}
}
