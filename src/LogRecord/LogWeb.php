<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Logit\LogRecord;

//
use Jantia\Logit\Exception\InvalidArgumentException;
use Tiat\Uri\Ip;
use Tiat\Uri\UriHost;

use function htmlspecialchars;
use function http_response_code;
use function implode;
use function in_array;
use function ksort;
use function serialize;
use function sprintf;
use function strtolower;
use function strtoupper;
use function trim;

/**
 *
 */
class LogWeb extends AbstractLogRecord {
	
	// Default web fields to detect
	public const DEFAULT_FIELDS = ['unique_id', 'http_user_agent', 'http_accept_language', 'http_referer', 'client_ip',
	                               'request_method', 'response_code', 'request_uri'];
	
	/**
	 * @var array
	 */
	private array $_fields = self::DEFAULT_FIELDS;
	
	/**
	 * @param    null|iterable    $_serverData
	 */
	public function __construct(private ?iterable $_serverData = NULL) {
		// Fill server data with default is any not given
		if(empty($this->_serverData)):
			$this->_serverData = $_SERVER;
		endif;
	}
	
	/**
	 * @param    array    $fields
	 *
	 * @return LogRecordInterface
	 */
	public function setFields(array $fields) : LogRecordInterface {
		//
		if($this->checkFields($fields) === TRUE):
			$this->_fields = $fields;
		else:
			$msg = sprintf("Fields must be in follow list: %s", implode(', ', $this->getSupportedFields()));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function getFields() : array {
		return $this->_fields;
	}
	
	/**
	 * @param    array    $fields
	 *
	 * @return bool
	 */
	public function checkFields(array $fields) : bool {
		if(! empty($fields)):
			$supported = $this->getSupportedFields();
			foreach($fields as $val):
				if(! in_array(strtolower($val), $supported, TRUE)):
					return FALSE;
				endif;
			endforeach;
		endif;
		
		//
		return TRUE;
	}
	
	/**
	 * @return string[]
	 */
	public function getSupportedFields() : array {
		return self::DEFAULT_FIELDS;
	}
	
	/**
	 * @return null|array
	 */
	public function getFieldInfo() : ?array {
		//
		if(! empty($fields = $this->getFields())):
			foreach($fields as $val):
				$result[$val] = $this->_getFieldInfo($val);
			endforeach;
			
			//
			ksort($result);
			
			//
			return ['extra' => $result];
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return mixed
	 */
	protected function _getFieldInfo(string $name) : mixed {
		return match ( strtolower(trim($name)) ) {
			'http_user_agent', 'http_accept_language', 'request_method', 'unique_id', 'http_referer' => $this->_getServerData($name),
			'response_code' => http_response_code(),
			'client_ip' => ( new Ip() )->clientIp(),
			'request_uri' => htmlspecialchars(( new UriHost($_SERVER) )->getUrl(), ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401, 'UTF-8'),
			default => NULL
		};
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return mixed
	 */
	protected function _getServerData(string $name) : mixed {
		return $this->_serverData[strtoupper($name)] ?? NULL;
	}
	
	/**
	 * @inheritDoc
	 */
	public function toString() : string {
		return $this->__toString();
	}
	
	/**
	 * @return string
	 */
	final public function __toString() : string {
		return serialize($this->getFieldInfo());
	}
}
