<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Logit\LogRecord;

//
use Tiat\Uri\UriHost;

/**
 * Set & resolve the HTTP hostname (not the server name in local network)
 */
class LogHostname extends AbstractLogRecord {
	
	/**
	 * @param    null|string    $_hostname
	 */
	public function __construct(private ?string $_hostname = NULL) {
		if($this->_hostname === NULL):
			$this->_hostname = ( new UriHost($_SERVER) )->getHost();
		endif;
	}
	
	/**
	 * @return null|null[]|string[]
	 */
	public function getHostname() : ?array {
		//
		if(! empty($this->_hostname)):
			return ['hostname'=>$this->_hostname];
		endif;
		
		//
		return null;
	}
	
	/**
	 * @return string
	 */
	public function toString() : string {
		return $this->__toString();
	}
	
	/**
	 * @return string
	 */
	final public function __toString() : string {
		return $this->_hostname;
	}
}
