<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Logit\LogRecord;

/**
 *
 */
interface LogRecordInterface {
	
	/**
	 * Attach Log Record data to message
	 *
	 * Return the serialized value
	 *
	 * @param    array|string    $message
	 *
	 * @return null|string
	 */
	public function attachToMessage(array|string $message) : ?string;
	
	/**
	 * Return the content as (serialized) string line
	 *
	 * @return string
	 */
	public function toString() : string;
}
