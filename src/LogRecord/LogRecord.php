<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Logit\LogRecord;

//
use Jantia\Logit\Exception\InvalidArgumentException;
use Jantia\Logit\LogitLevel;

use function base64_encode;
use function serialize;
use function strtolower;

/**
 *
 */
class LogRecord extends AbstractLogRecord {
	
	/**
	 * @param    LogitLevel    $_level
	 * @param    string        $_channel
	 * @param    string        $_timestamp
	 * @param    string        $_message
	 * @param    array         $_context
	 */
	public function __construct(private readonly LogitLevel $_level, private readonly string $_channel, private readonly string $_timestamp, private readonly string $_message, private readonly array $_context = []) {
	}
	
	/**
	 * @param    string    $key
	 *
	 * @return string|array|int
	 */
	public function get(string $key) : string|array|int {
		return match ( strtolower($key) ) {
			'level' => $this->_level->value,
			'channel' => $this->_channel,
			'timestamp' => $this->_timestamp,
			'message' => $this->_message,
			'context' => $this->_context,
			default => throw new InvalidArgumentException(sprintf("Given key (%s) is not valid", $key))
		};
	}
	
	/**
	 * @return array
	 */
	public function toArray() : array {
		//
		$result['headers'] = $this->_level->value . ' ' . $this->_timestamp . ' ' . $this->_channel . ' ';
		
		// Convert message to ARRAY
		$result['message'] = $this->_getDataAsArray($this->_message);
		
		//
		if(! empty($context = $this->_context['exception'] ?? NULL)):
			// If context have WeakMap inside then it can't be serialized directly
			$result['context'] = ( new LogException($context) )->toString();
		endif;
		
		//
		return $result;
	}
	
	/**
	 * Default message: level number + timestamp + channel + message (+ context in message if exists)
	 *
	 * @return string
	 */
	public function toString() : string {
		return $this->__toString();
	}
	
	/**
	 * Default message: level number + timestamp + channel + message (+ context in message if exists)
	 *
	 * Result will be BASE64 encoded
	 *
	 * @return string
	 */
	final public function __toString() : string {
		//
		$result  = $this->toArray();
		#$headers = $result['headers'];
		#unset($result['headers']);
		
		// Serialize the message because it's an array
		return base64_encode(serialize($result));
	}
}
