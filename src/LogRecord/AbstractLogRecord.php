<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Logit\LogRecord;

//
use function array_merge_recursive;
use function is_array;
use function serialize;
use function unserialize;

/**
 *
 */
abstract class AbstractLogRecord implements LogRecordInterface {
	
	/**
	 * @param    array|string    $message
	 *
	 * @return null|string
	 */
	public function attachToMessage(array|string $message) : ?string {
		// Get this class data as array
		$data = $this->_getDataAsArray($this->toString());
		
		// Convert message to array
		$message = $this->_getDataAsArray($message);
		
		//
		return serialize(array_merge_recursive($data, $message));
	}
	
	/**
	 * @param    array|string    $data
	 *
	 * @return array
	 */
	protected function _getDataAsArray(array|string $data) : array {
		if(is_array($data)):
			return $data;
		elseif(is_array($result = @unserialize($data, ['allowed_classes' => FALSE]))):
			return $result;
		endif;
		
		// Convert string to array
		return (array)$data;
	}
}
