<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Logit\LogRecord;

//
use Error;
use Exception;
use TypeError;

use function serialize;

/**
 *
 */
class LogException extends AbstractLogRecord {
	
	/**
	 * @param    Exception|TypeError|Error    $_exception
	 */
	public function __construct(private readonly Exception|TypeError|Error $_exception) {
	
	}
	
	/**
	 * @return string
	 */
	public function toString() : string {
		return $this->__toString();
	}
	
	/**
	 * @return string
	 */
	public function __toString() : string {
		$result = ['file' => $this->_exception->getFile(), 'line' => $this->_exception->getLine(),
		           'message' => $this->_exception->getMessage(), 'code' => $this->_exception->getCode(),
		           'trace' => $this->_exception->getTraceAsString()];
		
		//
		return serialize($result);
	}
}
