<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Logit\Exception;

//
use Throwable;

/**
 *
 */
interface ExceptionInterface extends Throwable {

}
