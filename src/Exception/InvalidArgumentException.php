<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Logit\Exception;

/**
 *
 */
class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface {

}
