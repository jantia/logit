<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Logit;

//
use DateTimeZone;
use Jantia\Logit\Exception\InvalidArgumentException;
use Jantia\Logit\LogRecord\LogRecord;
use Jantia\Logit\LogRecord\LogRecordInterface;
use Stringable;
use Tiat\Collection\Dto\DataTransferObject;
use Tiat\Collection\Envelope\Envelope;
use Tiat\Collection\Envelope\EnvelopeInterface;
use Tiat\Config\Config;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Stdlib\Parameters\ParametersPlugin;
use Tiat\Stdlib\Time\TimestampTrait;

use function ctype_graph;
use function define;
use function defined;
use function gettype;
use function is_int;
use function is_string;
use function sprintf;

/**
 * Logit backend main class.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Logger extends AbstractLogger implements ParametersPluginInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ParametersPlugin;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use TimestampTrait;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_records;
	
	/**
	 * @param    string    $_name
	 * @param              ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(private readonly string $_name, ...$args) {
		if(! defined('ID_TRACE') && isset($_SERVER['HTTP_X_JANTIA_TRACE_ID'])):
			define('ID_TRACE', $_SERVER['HTTP_X_JANTIA_TRACE_ID']);
		endif;
		
		// Set default timezone to UTC
		$this->setTimezone(new DateTimeZone('UTC'));
		
		//
		$this->_checkName();
		
		// Set params if any
		if(! empty($args)):
			$this->setParams($args);
		endif;
		
		// Register shutdown function which will save handlers content
		register_shutdown_function([$this, 'shutdown']);
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkName() : void {
		if(ctype_graph($this->getName()) === FALSE):
			$msg =
				sprintf("The name of the Logger can contain only visibly printable characters (no white space), '%s' given.",
				        $this->getName());
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : string {
		return $this->_name;
	}
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public function __destruct() {
		// It's possible that there can be records after the shutdown method so call _save() again just-for-sure
		if(! empty($this->_records)):
			#echo '<b><i>'.__METHOD__.'</i></b><br>';
			#var_dump($this->_records);
			$this->_save();
		endif;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _save() : void {
		if(! empty($this->_records)):
			foreach($this->_records as $key => $val):
				// If encryption is ON then encrypt the whole message before sending it
				$msg = $this->_encryptMessage($val);
				#echo 'Log message as string: '.$val->toString().'<br><br>';
				#echo 'Log message in Envelope as DTO<br><br>';
				#var_dump($val);
				#echo '<br><br>';
				
				if($val instanceof LogRecordInterface && $val->get('level') !== 200):
					#var_dump($val);
					#echo '<br><br>';
				endif;
				
				// Remove the record
				unset($this->_records[$key]);
			endforeach;
		endif;
	}
	
	/**
	 * @param    LogRecordInterface    $logRecord
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	private function _encryptMessage(LogRecordInterface $logRecord) : string {
		if($this->getEncrypt() === TRUE && 1 === 2):
			echo __METHOD__ . '<br>';
			echo 'Encrypt the message<br>';
			echo 'Test DTO Message bus<br>';
			
			// Create DTO (Message for the Envelope) at first
			$dto = new DataTransferObject(['message' => $logRecord->toString()]);
			#$dto->setHash($this->getParam(AsiRegisterValues::DEFAULT_HASH->value??$dto::DEFAULT_HASH))->generateHash();
			
			// Put DTO to Envelope
			if(( $c = $this->getParam('config')?->get(EnvelopeInterface::class) ) !== NULL):
				$config = new Config($c);
			endif;
			
			// Close the Envelope (with signature)
			$e = ( new Envelope([], $config ?? NULL) )->setDto($dto)->close();
			
			echo 'Envelope has been set<br>';
			var_dump($e);
			echo '<br><br>';
			
			// Close the Envelope
			//$e->;
			
			#$this->setMessageBus(new MessageBus());
			#$this->_getMessageBus()?->setMessage($dto);
			#$msg = $this->_getMessageBus()?->getMessage();
			#var_dump($envelope);
			
			// Return Envelope as string
			$r = $e->toString();
			#echo 'content of Envelope object: <br>';
			#var_dump($r);
			#echo '<br>';
			#echo '<br<br>End of '.__METHOD__.'<br>';
			return $r;
		endif;
		
		//
		return $logRecord->toString();
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void {
		$this->_save();
	}
	
	/**
	 * @param    mixed                $level
	 * @param    Stringable|string    $message
	 * @param    array                $context
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function log(mixed $level, Stringable|string $message, array $context = []) : void {
		// This is needed
		if(! $level instanceof LogitLevel):
			if(is_int($level) || is_string($level)):
				$level = $this->toLogitLevel($level);
			else:
				$msg = sprintf("$level is expected to be int, string or instance of %s. Got '%s'", LogitLevel::class,
				               gettype($level));
				throw new InvalidArgumentException($msg);
			endif;
		endif;
		
		// Add record to queue
		$this->addRecord($level, $message, $context);
	}
	
	/**
	 * @param    LogitLevel|int    $level
	 * @param    string            $message
	 * @param    array             $context
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function addRecord(LogitLevel|int $level, string $message, array $context = []) : bool {
		// Convert $level to instance of LogitLevel if not already
		if(! $level instanceof LogitLevel):
			$level = $this->toLogitLevel($level);
		endif;
		
		// Log level + timestamp + channel + message
		$record = ( new LogRecord($level, $this->getChannel($level), $this->getTimeStamp(), $message, $context ?? []) );
		
		// Add record to internal handler array which will write records to output
		$this->_records[] = $record;
		
		//
		return TRUE;
	}
	
	/**
	 * @param    LogitLevel    $level
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getChannel(LogitLevel $level) : ?string {
		return $this->getName();
		#return $this->getName() . '.' . $level->name;
	}
}
