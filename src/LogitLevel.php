<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Logit;

//
use Jantia\Logit\Exception\InvalidArgumentException;
use Tiat\Psr\Log\LogLevel;
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumInt;
use Tiat\Standard\DataModel\TraitEnum;

use function implode;
use function sprintf;
use function strtolower;

/**
 * Represents the log levels
 */
enum LogitLevel: int implements InterfaceEnum, InterfaceEnumInt {
	
	//
	use TraitEnum;
	
	/**
	 * Detailed debug information
	 */
	case DEBUG = 100;
	
	/**
	 * Interesting events
	 * Example the logs
	 */
	case INFO = 200;
	
	/**
	 * Uncommon events
	 */
	case NOTICE = 250;
	
	/**
	 * Exceptional occurrences that are not errors
	 * Examples: Use of deprecated APIs, poor use of an API,
	 * undesirable things that are not necessarily wrong.
	 */
	case WARNING = 300;
	
	/**
	 * Runtime errors
	 */
	case ERROR = 400;
	
	/**
	 * Critical conditions
	 * Example: Application component unavailable, unexpected exception.
	 */
	case CRITICAL = 500;
	
	/**
	 * Action must be taken immediately
	 * Example: Entire website down, database unavailable, etc.
	 * This should trigger the SMS alerts and wake you up.
	 */
	case ALERT = 550;
	
	/**
	 * Urgent alert.
	 */
	case EMERGENCY = 600;
	
	/**
	 * Mapping between levels numbers defined in RFC 5424 and Logit levels
	 */
	final public const RFC_5424_LEVELS = [7 => self::DEBUG, 6 => self::INFO, 5 => self::NOTICE, 4 => self::WARNING,
	                                      3 => self::ERROR, 2 => self::CRITICAL, 1 => self::ALERT,
	                                      0 => self::EMERGENCY];
	
	/**
	 * @param    string    $name
	 *
	 * @return LogitLevel
	 */
	public static function fromName(string $name) : LogitLevel {
		return match ( ( $name = strtolower($name) ) ) {
			'debug' => self::DEBUG,
			'info' => self::INFO,
			'notice' => self::NOTICE,
			'warning' => self::WARNING,
			'error' => self::ERROR,
			'critical' => self::CRITICAL,
			'alert' => self::ALERT,
			'emergency' => self::EMERGENCY,
			default => throw new InvalidArgumentException(sprintf("Given value (%s) is not valid level value (must be one of %s).",
			                                                      $name, implode(', ', self::getNames())))
		};
	}
	
	/**
	 * @param    int    $value
	 *
	 * @return null|LogitLevel
	 */
	public static function fromValue(int $value) : ?LogitLevel {
		return self::tryFrom($value);
	}
	
	/**
	 * @param    int    $value
	 *
	 * @return null|LogitLevel
	 */
	public static function fromRfc(int $value) : ?LogitLevel {
		return self::RFC_5424_LEVELS[$value] ?? NULL;
	}
	
	/**
	 * @return string
	 */
	public function toPsrLevel() : string {
		return match ( $this ) {
			self::DEBUG => LogLevel::DEBUG,
			self::INFO => LogLevel::INFO,
			self::NOTICE => LogLevel::NOTICE,
			self::WARNING => LogLevel::WARNING,
			self::ERROR => LogLevel::ERROR,
			self::CRITICAL => LogLevel::CRITICAL,
			self::ALERT => LogLevel::ALERT,
			self::EMERGENCY => LogLevel::EMERGENCY
		};
	}
	
	/**
	 * @return int
	 */
	public function toRfcLevel() : int {
		return match ( $this ) {
			self::DEBUG => 7,
			self::INFO => 6,
			self::NOTICE => 5,
			self::WARNING => 4,
			self::ERROR => 3,
			self::CRITICAL => 2,
			self::ALERT => 1,
			self::EMERGENCY => 0
		};
	}
	
	/**
	 * Pre-defined constants to LogLevel
	 *
	 * @param    int    $error
	 *
	 * @return null|string
	 */
	public static function fromError(int $error) : ?string {
		return match ( $error ) {
			E_ERROR, E_CORE_ERROR => LogLevel::CRITICAL,
			E_WARNING, E_CORE_WARNING, E_COMPILE_WARNING, E_USER_WARNING => LogLevel::WARNING,
			E_PARSE, E_COMPILE_ERROR => LogLevel::ALERT,
			E_NOTICE, E_USER_NOTICE, E_STRICT, E_DEPRECATED, E_USER_DEPRECATED => LogLevel::NOTICE,
			E_USER_ERROR, E_RECOVERABLE_ERROR => LogLevel::ERROR,
			default => NULL
		};
	}
	
	/**
	 * @return null|array
	 */
	public static function getNames() : ?array {
		//
		$array = self::toArray(self::cases());
		
		//
		foreach($array as $val):
			$result[] = self::fromValue($val)->name;
		endforeach;
		
		//
		return $result ?? NULL;
	}
}
