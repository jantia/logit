<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Logit;

//
use Jantia\Logit\Exception\InvalidArgumentException;
use Throwable;
use Tiat\Psr\Log\LoggerInterface;
use Tiat\Psr\Log\LogLevel;

use function error_reporting;
use function is_int;
use function is_string;
use function set_error_handler;
use function set_exception_handler;
use function sprintf;

/**
 *
 */
class ErrorHandler extends Error implements ErrorHandlerInterface {
	
	//
	final public const ERROR_HANDLER_METHOD = 'register';
	
	/**
	 * @var array
	 */
	private array $_errorLevelMap;
	
	/**
	 * @param    LoggerInterface    $logger
	 *
	 * @return void
	 */
	public function register(LoggerInterface $logger) : void {
		// Register default error handler
		$this->registerErrorHandler();
		
		// Register Exception handler
		$this->registerExceptionHandler();
	}
	
	/**
	 * @return $this
	 */
	public function registerErrorHandler() : static {
		//
		set_error_handler([$this, 'handleError'], E_ALL);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 */
	public function registerExceptionHandler() : static {
		//
		set_exception_handler(function (Throwable $e) : void {
			$this->_handleException($e);
		});
		
		//
		return $this;
	}
	
	/**
	 * @param    Throwable    $e
	 *
	 * @return never
	 */
	private function _handleException(Throwable $e) : never {
		//
		$level = LogLevel::ERROR;
		
		//
		$msg = sprintf("'%s' at file '%s' on line '%s'", $e->getMessage(), $e->getFile(), $e->getLine());
		
		//
		$this->log($level, $msg, ['exception' => $e]);
		
		//
		exit(255);
	}
	
	public function registerFatalHandler() : static {
		#register_shutdown_function([$this, 'handleFatalError']);
		echo 'Fatal detected<br>';
		
		return $this;
	}
	
	/**
	 * @param    int       $errno
	 * @param    string    $errstr
	 * @param    string    $errfile
	 * @param    int       $errline
	 *
	 * @return bool
	 */
	public function handleError(int $errno, string $errstr, string $errfile = '', int $errline = 0) : bool {
		//
		if(! ( error_reporting() & $errno )):
			return FALSE;
		endif;
		
		//
		$context = ['errno' => $errno, 'message' => $errstr, 'file' => $errfile, 'line' => $errline];
		
		//
		$this->log($errno, $errstr, $context);
		
		//
		return TRUE;
	}
	
	/**
	 * @param    LogitLevel|string|int    $level
	 *
	 * @return LogitLevel
	 */
	public function toLogitLevel(LogitLevel|string|int $level) : LogitLevel {
		// Return the LogitLevel instance right away
		if($level instanceof LogitLevel):
			return $level;
		elseif(is_string($level)):
			// Try to detect by name
			return LogitLevel::fromName($level);
		elseif(is_int($level)):
			// Try with direct error number from LogitLevel
			if(($t = LogitLevel::tryFrom($level)) === NULL):
				// Try pre-defined PHP constant(s) with RFC
				if(! empty($t = LogitLevel::fromError($level))):
					return LogitLevel::fromName($t);
				endif;

				//
				$a = implode(', ', [...LogitLevel::getNames(), ...LogitLevel::toArray(LogitLevel::cases())]);
				$msg = sprintf("Error with error number %s. Must be one of %s", $level, $a);
				throw new InvalidArgumentException($msg);
			endif;

			//
			return $t;
		endif;
		
		// As default throw exception
		throw new InvalidArgumentException(sprintf("Error %s is not supported.", $level));
	}
}
