<?php

/**
 * Jantia
 *
 * @package        Jantia/Logit
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Logit;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface ErrorHandlerInterface {
	
	/**
	 * Register default error handler
	 *
	 * @return ErrorHandlerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function registerErrorHandler() : ErrorHandlerInterface;
	
	/**
	 * Register Exception handler
	 *
	 * @return ErrorHandlerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function registerExceptionHandler() : ErrorHandlerInterface;
	
	/**
	 * @return ErrorHandlerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function registerFatalHandler() : ErrorHandlerInterface;
	
	/**
	 * Convert level to LogitLevel
	 *
	 * @param    LogitLevel|string|int    $level
	 *
	 * @return LogitLevel
	 * @since   3.0.0 First time introduced.
	 */
	public function toLogitLevel(LogitLevel|string|int $level) : LogitLevel;
	
	/**
	 * PHP error handler method which is called as default
	 *
	 * @param    int       $errno
	 * @param    string    $errstr
	 * @param    string    $errfile
	 * @param    int       $errline
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function handleError(int $errno, string $errstr, string $errfile = '', int $errline = 0) : bool;
}
